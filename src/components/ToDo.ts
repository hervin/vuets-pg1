import { Component, Vue } from "vue-property-decorator";
import WithRender from "./to-do.html";
import ToDoForm from "./ToDoForm";
import Task from "@/types/Task";
import { namespace } from "vuex-class";

const toDoModule = namespace("ToDoStore");

@WithRender
@Component({
  components: {
    "to-do-form": ToDoForm
  }
})
export default class ToDo extends Vue {
  @toDoModule.State
  public tasks!: Task[];

  @toDoModule.Mutation
  public addTask!: (description: string) => void 
}
