import Vue from 'vue';
import Vuex, {
  StoreOptions
} from 'vuex';
import RootState from '@/types/RootState';
import ToDoStore from "./ToDo";

Vue.use(Vuex);

const store: StoreOptions < RootState > = {
  state: {
    version: '1.0.0' // a simple property
  },
  modules: {
    ToDoStore
  }
};

export default new Vuex.Store < RootState > (store);