import { VuexModule, Module, Mutation } from "vuex-module-decorators";
import Task from "@/types/Task";

@Module({
  namespaced: true
})
class ToDoStore extends VuexModule {
  public tasks: Task[] = [
    { description: "Make Coffee", completed: false },
    { description: "Feed Dragons", completed: false }
  ];

  @Mutation
  addTask(description: string) {
    this.tasks.push({ description, completed: false });
  }
}

export default ToDoStore;
