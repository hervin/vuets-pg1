import { MutationTree } from 'vuex';
import PersonState  from '@/types/PersonState';

export const mutations: MutationTree<PersonState> = {
    personLoaded(state, payload: PersonState) {
        state.firstName = payload.firstName;
        state.lastName = payload.lastName;
        state.error = false
    },
    personError(state) {
        state.error = true;
        state.firstName = 'abc';
        state.lastName = '';
    }
};