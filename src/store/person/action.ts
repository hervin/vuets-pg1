import {
    ActionTree
} from 'vuex'
import RootState from '@/types/RootState'
import PersonState from '@/types/PersonState'

export const actions: ActionTree < PersonState, RootState > = {
    fetchData({
        commit,
        rootState,
        state,
        dispatch
    }): any {
        var b: PersonState = {
            error: false,
            firstName: 'John',
            lastName: 'Doe'
        }
        commit('personLoaded', b)
    }
};