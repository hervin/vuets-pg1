interface PersonState {
    error: boolean,
    firstName: string,
    middleName?: string,
    lastName: string,
    dateOfBirth?: Date
}

export default PersonState